
angular.module('seedApp')

.controller('IndexController',function($scope,byeService,VERSION) {
	$scope.span1="_HOME_PAGE_";
	
	$scope.bye=byeService.sayBye;
	
	$scope.version=VERSION;

})

.controller('LoginController',function($scope,$location) {
	$scope.view_name="_LOGIN_PAGE_";
	$scope.loginFunc=function() {
		console.log("___________"+$scope.username+'/'+$scope.password);
		$location.path('/about/?').search('username',$scope.username).search('password',$scope.password);		
	}
})

.controller('AboutController',function($scope,$location,helloService) {
	$scope.view_name="_ABOUT_PAGE_"
	
	var path=$location.path()
	var username=$location.search()['username']
	var password=$location.search()['password']
	
	console.log('Path: '+path)
	console.log('Username: '+username)
	console.log('Password: '+password)
	
	$scope.username=username
	$scope.password=password
	$scope.names=['aa','bb','cc']
	
	$scope.hello=helloService.sayHelloElapsed
	
})

