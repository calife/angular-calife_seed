angular.module('seedApp')
.service('helloService',function($timeout) {
	
	this.sayHello= function() {
		console.log("Hello!")
		window.alert('Hello!');
	}
	
	this.sayHelloElapsed= function() {
		$timeout(function() {
			console.log("Hello!")
			window.alert('Hello!');
		},2000)
	}
	
})
// Eager loading of service, caricamento in anticipo
.run(function(helloService) {
	var msg='helloService is ready'
	console.log(msg);
})
.run(function(byeService) {
	var msg='byeService is ready'
	console.log(msg);
})