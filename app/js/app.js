'use strict';

angular.module('seedApp',['ngRoute'])

.constant('VERSION','v 0.0.2')

.config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) {
	
	$routeProvider.when("/", {templateUrl:'partial/home.html'})
				  .when("/about/:some", {templateUrl:'partial/about.html',controller: 'AboutController'})
				  .when("/about", {templateUrl:'partial/about.html',controller: 'AboutController'})
				  .when("/login", {templateUrl:'partial/login.html',controller: 'LoginController'})
				  .when("/logout", {templateUrl:'partial/logout.html'})
				  .otherwise({template:'404 CODE'})
				  
    $locationProvider.html5Mode(true);
	
}])