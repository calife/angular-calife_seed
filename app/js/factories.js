angular.module('seedApp')

.factory('byeService', function() {
	return {
		sayBye: function() {
			console.log('Bye')
			return 'Bye';
		},
		sayByeBye: function() {
			console.log('Bye bye')
			return 'Bye bye';
		}
	}
	
})